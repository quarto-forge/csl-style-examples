# Citation Style Examples for Quarto

TL;DR https://quarto-forge.gitlab.io/csl-style-examples

[Quarto®](https://quarto.org/docs/websites) is an open-source scientific and technical publishing system built on [Pandoc](https://pandoc.org/).
Pandoc can generate citations and a bibliography if you provide the necessary files.
This repository has the source code of a website hosting a couple of examples of the different citation style render by Quarto.

## Add New Style

Edit the [styles.csv](styles.csv).

## Similar Projects

1. https://github.com/KurtPfeifle/pandoc-csl-testing